import numpy as np
import json

def sigma(x):
    return 1 / (1 + np.exp(-x))

def d_sigma(x):
    return 0.25 - (sigma(x) - 0.5) ** 2

def tanh(x):
    return np.tanh(x)

def d_tanh(x):
    return 1 - tanh(x) ** 2

def relu(x):
    return np.maximum(x, 0)

def d_relu(x):
    return np.maximum(np.sign(x), 0)

def softmax(pre_softmax, mask=None):
	x = pre_softmax
	if not mask is None:
		x[~mask] = -np.inf
	x -= np.max(x)
	e_x = np.exp(x)
	return e_x / np.sum(e_x, axis=tuple(range(1, e_x.ndim)), keepdims=True)

def toral_conv(inputs, kernel, origin, step=1, window=None):
    shape = inputs.shape
    if isinstance(step, int):
        step = np.array(inputs.ndim * [step])
    if window is None:
		window = inputs.shape
    outputs = np.zeros(window)
    for i in range(inputs.ndim):
        inputs = np.concatenate((inputs, inputs), axis=i)
    for index, value in np.ndenumerate(kernel):
        to_add = inputs * value
        for i in range(inputs.ndim):
            v = (step * (np.array(index) - np.array(origin)))[i] % shape[i]
            to_add = np.take(to_add, range(v, v + outputs.shape[i]), axis=i)
        outputs += to_add
    return outputs

def to_list_dict(ar):

	'''
	Returns a json serializable object.
	'''
	
	if isinstance(ar, (tuple, list)):
		return [to_list_dict(a) for a in ar]
	if isinstance(ar, dict):
		return {k: to_list_dict(v) for (k, v) in ar.items()}
	if isinstance(ar, np.ndarray):
		return ar.tolist()
	return ar

class Layer:
    class Params:
        def __init__(self, W_shape, b_shape, inputs_shape, outputs_shape):
            self.W = np.zeros(W_shape)
            self.b = np.zeros(b_shape)
            self.inputs = np.zeros(inputs_shape)
            self.outputs = np.zeros(outputs_shape)
            self.n_params = self.W.size + self.b.size
        
        def get(self):
            return {'W': self.W, 'b': self.b}
        
        def set(self, W=None, b=None, inputs=None, outputs=None):
            if W is not None:
                self.W = np.array(W)
            if b is not None:
                self.b = np.array(b)
        
        def clear(self):
            self.W.fill(0)
            self.b.fill(0)
            
        def total_square(self):
            return np.sum(self.W ** 2) + np.sum(self.b ** 2)
            
    
    def __init__(self, W_shape, b_shape, inputs_shape, outputs_shape):
        self.params = self.Params(W_shape, b_shape, inputs_shape, outputs_shape)
        self.grads = self.Params(W_shape, b_shape, inputs_shape, outputs_shape)
        self.grads_saved = self.Params(W_shape, b_shape, inputs_shape, outputs_shape)

    def update(self, lr, mom=0.0):
        self.grads_saved.W = mom * self.grads_saved.W + (1 - mom) * self.grads.W
        self.grads_saved.b = mom * self.grads_saved.b + (1 - mom) * self.grads.b
        self.params.W -= lr * self.grads_saved.W
        self.params.b -= lr * self.grads_saved.b

    def accumulate(self):
        self.grads_saved.W += self.grads.W
        self.grads_saved.b += self.grads.b

						

class ToralConvLayer(Layer):
	def __init__(self, height, width, kH, kW, orH, orW, n_in=1, n_out=1, size=1e-3):
		Layer.__init__(self, (n_in, n_out, kH, kW), (n_in, n_out), (0, n_in, height, width), (0, n_out, height, width))
		self.height = height
		self.width = width
		self.kH = kH
		self.kW = kW
		self.k_origin = np.array([orH, orW])
		self.n_in = n_in
		self.n_out = n_out
		self.params.W = np.random.randn(*self.params.W.shape) * size
		self.params.b = np.random.randn(*self.params.b.shape) * size

	def forward(self, inputs):
		self.params.inputs = inputs
		if inputs.ndim == 3:
			inputs = inputs.reshape([1] + list(inputs.shape))
		self.params.outputs = np.zeros((inputs.shape[0], self.n_out, self.height, self.width))
		for a in range(self.n_in):
			for b in range(self.n_out):
				self.params.outputs[:, b] += np.array([toral_conv(inputs[i, a], self.params.W[a, b], self.k_origin) for i in range(inputs.shape[0])]) + self.params.b[a, b]
		if self.params.inputs.ndim == 3:
			self.params.outputs = self.params.outputs[0]
		return self.params.outputs

	def backward(self, d_outputs):
		self.grads.outputs = d_outputs
		if d_outputs.ndim == 3:
			d_outputs = d_outputs.reshape([1] + list(d_outputs.shape))
		self.grads.outputs = d_outputs
		self.grads.inputs = np.zeros((d_outputs.shape[0], self.n_in, self.height, self.width))
		self.grads.W.fill(0)
		for a in range(self.n_in):
			for b in range(self.n_out):
				self.grads.inputs[:, a] += np.array([toral_conv(d_outputs[i, b], self.params.W[a, b], self.k_origin, step=-1) for i in range(d_outputs.shape[0])])
				self.grads.W[a, b] = np.mean(np.array([toral_conv(d_outputs[i, b], self.params.inputs[i, a], self.k_origin, step=-1, window=(self.kH, self.kW)) for i in range(d_outputs.shape[0])]), axis=0)
				self.grads.b[a, b] = np.mean(np.sum(d_outputs[:, b], axis=(1, 2)))
		if d_outputs.ndim == 3:
			self.grads.inputs = self.grads.inputs[0]
		return self.grads.inputs



class FullConn(Layer):
	def __init__(self, inputs_size, outputs_size, size=1e-3):
		Layer.__init__(self, (input_size, outputs_size), outputs_size, inputs_size, outputs_size)
		self.params.W = np.random.randn(*self.params.W.shape) * size
		self.params.b = np.random.randn(*self.params.b.shape) * size
		self.inputs_size = inputs_size
		self_outputs_size = outputs_size
		
	def forward(self, inputs):
		self.params.inputs = inputs
		self.params.outputs = self.params.inputs.dot(self.params.W) + self.params.b
		return self.params.outputs
		
	def backward(self, d_outputs):
		self.grads.outputs = d_outputs
		self.grads.b = self.grads.outputs[:]
		self.grads.W = self.params.inputs.outer(self.grads.outputs)
		self.grads.inputs = self.params.W.dot(self.grads.outputs)



class Reshape(Layer):
	def __init__(self, inputs_shape, outputs_shape):
		Layer.__init__(self, (0,), (0,), inputs_shape, outputs_shape)
		assert self.params.inputs.size == self.params.outputs.size, 'Inputs size (%d) and outputs size (%d) are not the same' % (self.params.inputs.size, self.params.outputs.size)
		self.inputs_shape = inputs_shape
		self.outputs_shape = outputs_shape
		
	def forward(self, inputs):
		self.params.inputs = inputs
		assert inputs.shape[-len(self.inputs_shape):] == self.inputs_shape
		batch_shape = inputs.shape[:-len(self.inputs_shape)]
		self.params.outputs = inputs.reshape(batch_shape + self.outputs_shape)
		return self.params.outputs
		
	def backward(self, d_outputs):
		self.grads.outputs = d_outputs
		assert d_outputs.shape[-len(self.outputs_shape):] == self.outputs_shape
		batch_shape = d_outputs.shape[:-len(self.outputs_shape)]
		self.grads.inputs = d_outputs.reshape(batch_shape + self.inputs_shape)
		return self.grads.inputs



class ReLU(Layer):
    def __init__(self):
        Layer.__init__(self, (0), (0), (0), (0))

    def forward(self, inputs):
        self.params.inputs = inputs
        self.params.outputs = relu(inputs)
        return self.params.outputs

    def backward(self, d_outputs):
        self.grads.outputs = d_outputs
        self.grads.inputs = d_relu(self.params.inputs) * d_outputs
        return self.grads.inputs



class Sigmoid(Layer):
    def __init__(self):
        Layer.__init__(self, (0), (0), (0), (0))

    def forward(self, inputs):
        self.params.inputs = inputs
        self.params.outputs = sigma(inputs)
        return self.params.outputs

    def backward(self, d_outputs):
        self.grads.outputs = d_outputs
        self.grads.inputs = d_sigma(self.params.inputs) * d_outputs
        return self.grads.inputs



class Tanh(Layer):
    def __init__(self):
        Layer.__init__(self, (0), (0), (0), (0))

    def forward(self, inputs):
        self.params.inputs = inputs
        self.params.outputs = tanh(inputs)
        return self.params.outputs

    def backward(self, d_outputs):
        self.grads.outputs = d_outputs
        self.grads.inputs = d_tanh(self.params.inputs) * d_outputs
        return self.grads.inputs



class Sum(Layer):
	def __init__(self):
		Layer.__init__(self, (1), (1), (0), (1))
		
	def forward(self, inputs):
		self.params.inputs = inputs
		if inputs.ndim == 1:
			inputs = inputs[None]
		self.params.outputs = np.sum(inputs * self.params.W, axis=1) + self.params.b
		if inputs.ndim == 1:
			self.params.outputs = self.params.outputs[0]
		return self.params.outputs
		
	def backward(self, d_outputs):
		self.grads.outputs = d_outputs
		self.grads.b = np.mean(self.grads.outputs)
		self.grads.W = np.mean(np.sum(self.params.inputs, axis=1) * d_outputs)
		self.grads.inputs = np.expand_dims((self.params.W * d_outputs), axis=1) + np.zeros(self.params.inputs.shape)
		return self.grads.inputs


class NeuralNet:
    def __init__(self):
        self.layers = []
        self.rolling_mean_square = 0
        self.n_params = 0

    def add(self, layer):
        self.layers.append(layer)
        self.n_params += layer.params.n_params

    def forward(self, inputs):
        outputs = inputs
        for layer in self.layers:
            outputs = layer.forward(outputs)
        return outputs

    def backward(self, d_outputs):
        d_inputs = d_outputs
        for layer in self.layers[::-1]:
            d_inputs = layer.backward(d_inputs)
        return d_inputs

    def update(self, lr, alpha=0.9, epsilon=1e-4, mom=0.0):
		self.rolling_mean_square = alpha * self.rolling_mean_square + (1 - alpha) * self.grad_mean_square()
		for layer in self.layers:
			layer.update(lr / np.sqrt(self.rolling_mean_square + epsilon), mom)

    def get_params(self):
        return [layer.params.get() for layer in self.layers]

    def set_params(self, list_of_params):
        assert len(list_of_params) == len(self.layers)
        for i in range(len(self.layers)):
            self.layers[i].params.set(**list_of_params[i])

    def get_grads(self):
        return [layer.grads.get() for layer in self.layers]

    def set_grads(self, list_of_grads):
        assert len(list_of_grads) == len(self.layers)
        for i in range(len(self.layers)):
            self.layers[i].grads.set(list_of_grads[i])

    def save_params(self, path):
        to_save = self.get_params()
        to_save = to_list_dict(to_save)
        file = open(path, 'wb')
        json.dump(to_save, file)

    def load_params(self, path):
        file = open(path, 'rb')
        to_load = json.load(file)
        self.set_params(to_load)

    def save_grads(self, path):
        to_save = self.get_grads()
        to_save = to_list_dict(to_save)
        file = open(path, 'wb')
        json.dump(to_save, file)

    def load_grads(self, path):
        file = open(path, 'rb')
        to_load = json.load(file)
        self.set_grads(to_load)
        
    def grad_mean_square(self):
		return np.sum(np.array([layer.grads.total_square() for layer in self.layers])) / self.n_params


def mse(outputs, targets):
	return np.mean(outputs - targets) ** 2 / 2
	
def d_mse(outputs, targets):
	return outputs - targets	

	

