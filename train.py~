import numpy as np
import copy
import neural_net as nn
from game import *
import os
import json
import argparse
import sys

def namespace_to_string(n):
	rtn = str(n)
	rtn = rtn[rtn.find('(') + 1 : rtn.find(')')]
	rtn = rtn.replace(' ', '_').replace(',', '').replace("'", '')
	return rtn

parser = argparse.ArgumentParser()
parser.add_argument("network_type", type=str, help="type of network to train", choices=['Q', 'V'])
parser.add_argument("n_filters", type=int, help="numbers of filters in hidden layers", nargs="+")
parser.add_argument('--seed', type=int, help='seed for pseudorandom initialization, default = 0', default = 0)
parser.add_argument('--size', type=float, help='size for initialization, default=0.1', default=0.1)
#parser.add_argument('--load', action='store_true', help='load parameters from file')
parser.add_argument('--epsilon', type=float, help='initial exploration epsilon, default = 1.0', default = 1.0)
parser.add_argument('--epsilon_min', type=float, help='minimal exploration epsilon, default = 0.1', default = 0.1)
parser.add_argument('--delta', type=float, help='decrement of exploration epsilon per epoch, default = 1e-3', default = 1e-3)
parser.add_argument('--gamma', type=float, help='fuure reward discount, default = 0.9', default = 0.9)
parser.add_argument('--lr', type=float, help='learning rate for RMSprop algorithm, default = 3e-4', default = 3e-4)
parser.add_argument('--n_batch', type=int, help='lower bound for batch size, default = 100', default = 100)
parser.add_argument('--n_epochs', type=int, help='number of training epochs, default = 1500', default = 1500)

args = parser.parse_args()
	
for a in args.n_filters:
	if a <= 0:
		parser.error('n_filters must be a sequence of positive integers.')

if args.seed is not None:
	np.random.seed(args.seed)
path = 'save/'

n_filters = [1] + args.n_filters + [1]
print n_filters

network_type = args.network_type

if network_type == 'Q':
	net = nn.NeuralNet()
	net.add(nn.Reshape((6, 6), (1, 6, 6)))
	for i in range(len(n_filters) - 1):
		net.add(nn.ToralConvLayer(6, 6, 3, 3, 1, 1, n_filters[i], n_filters[i + 1], size=1e-1))
		if i < len(n_filters) - 2:
			net.add(nn.ReLU())
		else:
			net.add(nn.Reshape((1, 6, 6), (6, 6)))
			net.add(nn.Tanh())

if network_type == 'V':
	net = nn.NeuralNet()
	net.add(nn.Reshape((6, 6), (1, 6, 6)))
	for i in range(len(n_filters) - 1):
		if i < len(n_filters) - 2:
			net.add(nn.ToralConvLayer(6, 6, 3, 3, 1, 1, n_filters[i], n_filters[i + 1], size=1e-1))
			net.add(nn.ReLU())
		else:
			net.add(nn.Reshape((n_filters[i], 6, 6), (n_filters[i] * 6 * 6,)))
			net.add(nn.Sum())
			net.add(nn.Tanh())

epsilon = args.epsilon
epsilon_min = args.epsilon_min
gamma = args.gamma
delta = args.delta
lr = args.lr
n_batch = args.n_batch
n_epochs = args.n_epochs
first_player = True
epoch = 0
if not 'save' in os.listdir('.'):
    os.mkdir('save')
states_batch = np.zeros((0, 6, 6))
actions_batch = np.zeros((0, 6, 6))
target_batch = np.zeros((0,))
while epoch < n_epochs:
    print ('Epoch %d, epsilon = %.4f' % (epoch, epsilon))
    val = net.forward(np.zeros((6, 6)))
    while isinstance(val, np.ndarray):
		val = val[0]
    print (network_type + '(empty_board) = %.6f' % val)
    first_player = not first_player
    result, [states, _], [actions, _] = gameplay([net, None], [network_type] * 2, [epsilon, 1.0], first_player=first_player, hard=(False, False), print_outputs=(True, False))
    target = np.ones(states.shape[0]) * result
    for i in range(target.shape[0] - 1)[::-1]:
        target[i] = gamma * target[i + 1]
    print ('Updating training data...')
    states_batch = np.concatenate((states_batch, states))
    actions_batch = np.concatenate((actions_batch, actions))
    target_batch = np.concatenate((target_batch, target))
    print ('Done. Batch size = %d.' % target_batch.shape[0])
    if target_batch.shape[0] > n_batch:
        print ('Forward pass...')
        if network_type == 'Q':
			d_outputs = net.forward(states_batch) - target_batch.reshape(target_batch.shape + (1, 1))
			d_outputs[actions_batch == 0] = 0
		if network_type == 'V':
			d_outputs = net.forward(states_batch + actions_batch) - target_batch
        print ('Done. Backward pass...')
        net.backward(d_outputs)
        print ('Done. Updating...')
        net.update(lr)
        print ('Done. Saving the network...')
        net.save_params(path + network_type + '_params.json')
        print 'Saved.'
        epsilon = np.maximum(epsilon_min, epsilon - delta)
        epoch += 1
        states_batch = np.zeros((0, 6, 6))
        actions_batch = np.zeros((0, 6, 6))
        target_batch = np.zeros((0,))
                
print ('Saving the network...')
net.save_params(path + 'params_' + namespace_to_string(args) + '.json')
print 'Saved.'
