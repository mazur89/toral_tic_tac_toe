import numpy as np
import neural_net as nn

board = np.zeros((6, 6))

def legal_moves(board):
	rtn = np.zeros((np.sum(board == 0),) + board.shape)
	counter = 0
	for idx, val in np.ndenumerate(board == 0):
		if val:
			rtn[(counter,) + idx] = 1
			counter += 1
	return rtn
	

def random_move(board):
    legal_moves = np.sum(board == 0)
    move = int(np.random.uniform() * legal_moves)
    counter = 0
    for idx, val in np.ndenumerate(board == 0):
        if counter == move and val:
            break
        if val:
            counter += 1
    rtn = np.zeros(board.shape)
    rtn[idx] = 1
    return rtn
    
def hard_move(board):
	score = np.zeros(board.shape)
	for a in range(6):
		for(b) in range(6):
			for d in [np.array([1, 0]), np.array([0, 1]), np.array([1, 1]), np.array([1, -1])]:
				line = np.zeros(6)
				for k in range(6):
					line[k] = board[tuple(([a, b] + k * d) % 6)]
				pos = np.sum(line > 0)
				neg = np.sum(line < 0)
				if neg < 2:
					score[a, b] += 5 ** (2 * pos + 1)
				if pos < 2:
					score[a, b] += 5 ** (2 * neg)
	score[board != 0] = -1
	rtn = np.zeros(board.shape)
	max_score = np.max(score)
	n_max = np.sum(score == max_score)
	u = np.random.uniform() * n_max
	counter = 0
	for idx, val in np.ndenumerate(score == max_score):
		if val:
			counter += 1
			if counter >= u:
				break
	rtn[idx] = 1
	return rtn
				
def victory(board):
    rtn = False
    for a in range(6):
        for b in range(6):
            for d in [np.array([1, 0]), np.array([0, 1]), np.array([1, 1]), np.array([1, -1])]:
                t = True
                for k in range(5):
                    if board[tuple(([a, b] + k * d) % 6)] <= 0:
                        t = False
                rtn = rtn or t
    return rtn

def if_full(board):
    legal_moves = np.sum(board == 0)
    return legal_moves == 0

def game_over(board):
    return victory(board) or victory(-board) or if_full(board)

def gameplay(nets=[None, None], types=['Q', 'Q'], epsilons=[1.0, 1.0], first_player=True, board = np.zeros((6, 6)), print_outputs=(True, False), hard=(False, False)):
	states = [[], []]
	actions = [[], []]
	if first_player:
		print 'Playing as first player.'
		current = 0
	else:
		print 'Playing as second player.'
		current = 1
	while not game_over(board):
		u = np.random.uniform()
		if u <= epsilons[current]:
			if hard[current]:
				move = hard_move(board)
			else:
				move = random_move(board)
		else:
			if types[current] == 'Q':
				outputs = nets[current].forward(board)
				outputs[board != 0] = np.nan
			elif types[current] == 'V':
				legal = legal_moves(board)
				next_boards = board + legal
				values = nets[current].forward(next_boards)
				outputs = np.zeros(board.shape)
				outputs[board != 0] = np.nan
				for i in range(values.shape[0]):
					outputs[legal[i] == 1] = values[i]
			move = np.zeros(board.size)
			a = np.nanargmax(outputs)
			move[a] = 1
			move = move.reshape(board.shape)
			np.set_printoptions(precision=4, suppress=True)
			if print_outputs[current]:
				print 'board:'
				print board
				print 'outputs:'
				print outputs
				print 'move:'
				print move
		states[current].append(board)
		actions[current].append(move)
		board = board + move
		current = (current + 1) % 2
		board = -board
	if current == 1:
		current = 0
		board = -board
	if victory(board):
		print 'Game won.'
		result = 1
	elif victory(-board):
		print 'Game lost.'
		result = -1
	else:
		print 'Game drawn.'
		result = 0
	print '%d fields filled' % np.sum(board != 0)
	states = [np.array(s) for s in states]
	actions = [np.array(a) for a in actions]
	return result, states, actions

