import json
import numpy as np

path = 'save/epoch_29_params.json'
file = open(path, 'rb')
params = json.load(file)
path = 'save/epoch_29_grads.json'
file = open(path, 'rb')
grads = json.load(file)
board_before = np.array(params[0]['inputs'][6][0])
board_after = np.array(params[0]['inputs'][7][0])
print board_before
print board_after
print np.array(params[-1]['outputs'][6][0])
#print grads[-1]['outputs'][6][0]