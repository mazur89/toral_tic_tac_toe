import numpy as np
import neural_net as nn
from game import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("file_name", type=str, help="file with network to test")
parser.add_argument('--seed', type=int, help='seed for pseudorandom initialization, default = 0', default = 0)

args = parser.parse_args()
s = args.file_name
network_type = s[s.find('network_type=') + len('network_type=')]
n_filters = [1] + [int(x) for x in s[s.find('[') + 1 : s.find(']')].split('_')] + [1]


np.random.seed(args.seed)

if network_type == 'Q':
	net = nn.NeuralNet()
	net.add(nn.Reshape((6, 6), (1, 6, 6)))
	for i in range(len(n_filters) - 1):
		net.add(nn.ToralConvLayer(6, 6, 3, 3, 1, 1, n_filters[i], n_filters[i + 1], size=1e-1))
		if i < len(n_filters) - 2:
			net.add(nn.ReLU())
		else:
			net.add(nn.Reshape((1, 6, 6), (6, 6)))
			net.add(nn.Tanh())

if network_type == 'V':
	net = nn.NeuralNet()
	net.add(nn.Reshape((6, 6), (1, 6, 6)))
	for i in range(len(n_filters) - 1):
		if i < len(n_filters) - 2:
			net.add(nn.ToralConvLayer(6, 6, 3, 3, 1, 1, n_filters[i], n_filters[i + 1], size=1e-1))
			net.add(nn.ReLU())
		else:
			net.add(nn.Reshape((n_filters[i], 6, 6), (n_filters[i] * 6 * 6,)))
			net.add(nn.Sum())
			net.add(nn.Tanh())

net.load_params('save/' + s)

n_games = 1000
results = {-1: 0, 0: 0, 1: 0}
first_player = True
for i in range(n_games):
	print 'Game %d' % i
	first_player = not first_player
	result, _, _ = gameplay([net, None], [network_type] * 2, [0.0, 1.0], first_player=first_player, print_outputs=[False, False], hard=(False, False))
	results[result] += 1
	print ('%d games won,' % results[1])
	print ('%d games drawn,' % results[0])
	print ('%d games lost.' % results[-1])
