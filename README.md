# README #

This repository is for training neural networks to play the tic-tac-toe game with the board being the 2-dimensional torus with 6x6 fields and the winning condition is to make a line of 5. The opponent always plays randomly.

All neural networks are convolutional and are trained with multiple-step Q-learning. You can choose the following options:

* Types of network:

    * 'Q': for each state of the board estimates the value of all possible moves (having no idea what the move even does),
    * 'V': estimates the value of all possible boards you can get AFTER making the move (has the information what is the result of making the move). 

* Numbers of filters in hidden layers.

There are also optional parameters you can play with. They are:

* lr (learning rate),
* epsilon (initial exploration epsilon),
* epsilon_min (lower bound on exploration epsilon),
* delta (decrement of exploration epsilon per epoch),
* gamma (future reward discount)
* n_batch (lower bound on training batch size),
* n_epochs (number of training epochs),
* seed (random seed),
* size (size of random initialization of the network).

To start training, type in the command line:
python train.py [network_type] [n_filters]

Example:
python train.py Q 8 8 8 8 8

This will train the Q network with 5 hidden layers, each with 8 filters. After training it will save the network parameters into the 'save' folder. The filename will contain the values of all the initial parameters, including optional ones. In this case:

params_delta=0.001_epsilon=1.0_epsilon_min=0.1_gamma=0.9_lr=0.0003_n_batch=100_n_epochs=1500_n_filters=[8_8_8_8_8]_network_type=Q_seed=0_size=0.1.json

You can also test your network by playing 1000 games against the random opponent. To do this, just type:

python test.py [filename]

while in the main directory. The results will be printed on the screen. For example, typing:

python test.py params_delta=0.001_epsilon=1.0_epsilon_min=0.1_gamma=0.9_lr=0.0003_n_batch=100_n_epochs=1500_n_filters=[8_8_8_8_8]_network_type=Q_seed=0_size=0.1.json

results in printing:

910 games won,
0 games drawn,
90 games lost.